import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _df8eb26c = () => interopDefault(import('../pages/404.vue' /* webpackChunkName: "pages/404" */))
const _d9bd1716 = () => interopDefault(import('../pages/coleccion.vue' /* webpackChunkName: "pages/coleccion" */))
const _71d13bb8 = () => interopDefault(import('../pages/coleccion-interior.vue' /* webpackChunkName: "pages/coleccion-interior" */))
const _435418ad = () => interopDefault(import('../pages/contacto.vue' /* webpackChunkName: "pages/contacto" */))
const _3ef32e9d = () => interopDefault(import('../pages/nosotros.vue' /* webpackChunkName: "pages/nosotros" */))
const _08ca6e90 = () => interopDefault(import('../pages/nosotros-interior.vue' /* webpackChunkName: "pages/nosotros-interior" */))
const _09fc8720 = () => interopDefault(import('../pages/proyectos.vue' /* webpackChunkName: "pages/proyectos" */))
const _4f4e0d26 = () => interopDefault(import('../pages/proyectos-interior.vue' /* webpackChunkName: "pages/proyectos-interior" */))
const _5d994964 = () => interopDefault(import('../pages/puntos-proyectos.vue' /* webpackChunkName: "pages/puntos-proyectos" */))
const _97de8e48 = () => interopDefault(import('../pages/sustentabilidad.vue' /* webpackChunkName: "pages/sustentabilidad" */))
const _f61514d8 = () => interopDefault(import('../pages/testcar.vue' /* webpackChunkName: "pages/testcar" */))
const _26e40cc8 = () => interopDefault(import('../pages/testcpath.vue' /* webpackChunkName: "pages/testcpath" */))
const _66a2d415 = () => interopDefault(import('../pages/testpath.vue' /* webpackChunkName: "pages/testpath" */))
const _6dcf57ed = () => interopDefault(import('../pages/testpath2.vue' /* webpackChunkName: "pages/testpath2" */))
const _01047d22 = () => interopDefault(import('../pages/testsimple.vue' /* webpackChunkName: "pages/testsimple" */))
const _ef405a38 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/404",
    component: _df8eb26c,
    name: "404"
  }, {
    path: "/coleccion",
    component: _d9bd1716,
    name: "coleccion"
  }, {
    path: "/coleccion-interior",
    component: _71d13bb8,
    name: "coleccion-interior"
  }, {
    path: "/contacto",
    component: _435418ad,
    name: "contacto"
  }, {
    path: "/nosotros",
    component: _3ef32e9d,
    name: "nosotros"
  }, {
    path: "/nosotros-interior",
    component: _08ca6e90,
    name: "nosotros-interior"
  }, {
    path: "/proyectos",
    component: _09fc8720,
    name: "proyectos"
  }, {
    path: "/proyectos-interior",
    component: _4f4e0d26,
    name: "proyectos-interior"
  }, {
    path: "/puntos-proyectos",
    component: _5d994964,
    name: "puntos-proyectos"
  }, {
    path: "/sustentabilidad",
    component: _97de8e48,
    name: "sustentabilidad"
  }, {
    path: "/testcar",
    component: _f61514d8,
    name: "testcar"
  }, {
    path: "/testcpath",
    component: _26e40cc8,
    name: "testcpath"
  }, {
    path: "/testpath",
    component: _66a2d415,
    name: "testpath"
  }, {
    path: "/testpath2",
    component: _6dcf57ed,
    name: "testpath2"
  }, {
    path: "/testsimple",
    component: _01047d22,
    name: "testsimple"
  }, {
    path: "/",
    component: _ef405a38,
    name: "index"
  }, {
    path: "/coleccion/detalle",
    components: {
      default: _71d13bb8
    }
  }, {
    path: "/proyectos/:section",
    components: {
      default: _4f4e0d26
    }
  }, {
    path: "/proyectos/:section/:detalle",
    components: {
      default: _4f4e0d26
    }
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
