import { wrapFunctional } from './utils'

export { default as EndkCursor } from '../../components/endkCursor.vue'
export { default as EndkCurveText } from '../../components/endkCurveText.vue'
export { default as EndkDataScroll } from '../../components/endkDataScroll.vue'
export { default as EndkLargePath } from '../../components/endkLargePath.vue'
export { default as EndkMenu } from '../../components/endkMenu.vue'
export { default as EndkSimpleCar } from '../../components/endkSimpleCar.vue'
export { default as EndkSmallTexture } from '../../components/endkSmallTexture.vue'
export { default as SectionsEndkSection3 } from '../../components/sections/endk-section-3.vue'
export { default as SectionsEndkSectionColeccion } from '../../components/sections/endk-section-coleccion.vue'
export { default as SectionsEndkSectionContacto } from '../../components/sections/endk-section-contacto.vue'
export { default as SectionsEndkSectionInicio } from '../../components/sections/endk-section-inicio.vue'
export { default as SectionsEndkSectionMitigacion } from '../../components/sections/endk-section-mitigacion.vue'
export { default as SectionsEndkSectionNosotros } from '../../components/sections/endk-section-nosotros.vue'
export { default as SectionsEndkSectionProyectos } from '../../components/sections/endk-section-proyectos.vue'

export const LazyEndkCursor = import('../../components/endkCursor.vue' /* webpackChunkName: "components/endk-cursor" */).then(c => wrapFunctional(c.default || c))
export const LazyEndkCurveText = import('../../components/endkCurveText.vue' /* webpackChunkName: "components/endk-curve-text" */).then(c => wrapFunctional(c.default || c))
export const LazyEndkDataScroll = import('../../components/endkDataScroll.vue' /* webpackChunkName: "components/endk-data-scroll" */).then(c => wrapFunctional(c.default || c))
export const LazyEndkLargePath = import('../../components/endkLargePath.vue' /* webpackChunkName: "components/endk-large-path" */).then(c => wrapFunctional(c.default || c))
export const LazyEndkMenu = import('../../components/endkMenu.vue' /* webpackChunkName: "components/endk-menu" */).then(c => wrapFunctional(c.default || c))
export const LazyEndkSimpleCar = import('../../components/endkSimpleCar.vue' /* webpackChunkName: "components/endk-simple-car" */).then(c => wrapFunctional(c.default || c))
export const LazyEndkSmallTexture = import('../../components/endkSmallTexture.vue' /* webpackChunkName: "components/endk-small-texture" */).then(c => wrapFunctional(c.default || c))
export const LazySectionsEndkSection3 = import('../../components/sections/endk-section-3.vue' /* webpackChunkName: "components/sections-endk-section3" */).then(c => wrapFunctional(c.default || c))
export const LazySectionsEndkSectionColeccion = import('../../components/sections/endk-section-coleccion.vue' /* webpackChunkName: "components/sections-endk-section-coleccion" */).then(c => wrapFunctional(c.default || c))
export const LazySectionsEndkSectionContacto = import('../../components/sections/endk-section-contacto.vue' /* webpackChunkName: "components/sections-endk-section-contacto" */).then(c => wrapFunctional(c.default || c))
export const LazySectionsEndkSectionInicio = import('../../components/sections/endk-section-inicio.vue' /* webpackChunkName: "components/sections-endk-section-inicio" */).then(c => wrapFunctional(c.default || c))
export const LazySectionsEndkSectionMitigacion = import('../../components/sections/endk-section-mitigacion.vue' /* webpackChunkName: "components/sections-endk-section-mitigacion" */).then(c => wrapFunctional(c.default || c))
export const LazySectionsEndkSectionNosotros = import('../../components/sections/endk-section-nosotros.vue' /* webpackChunkName: "components/sections-endk-section-nosotros" */).then(c => wrapFunctional(c.default || c))
export const LazySectionsEndkSectionProyectos = import('../../components/sections/endk-section-proyectos.vue' /* webpackChunkName: "components/sections-endk-section-proyectos" */).then(c => wrapFunctional(c.default || c))
