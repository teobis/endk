import Vue from 'vue'
import { wrapFunctional } from './utils'

const components = {
  EndkCursor: () => import('../../components/endkCursor.vue' /* webpackChunkName: "components/endk-cursor" */).then(c => wrapFunctional(c.default || c)),
  EndkCurveText: () => import('../../components/endkCurveText.vue' /* webpackChunkName: "components/endk-curve-text" */).then(c => wrapFunctional(c.default || c)),
  EndkDataScroll: () => import('../../components/endkDataScroll.vue' /* webpackChunkName: "components/endk-data-scroll" */).then(c => wrapFunctional(c.default || c)),
  EndkLargePath: () => import('../../components/endkLargePath.vue' /* webpackChunkName: "components/endk-large-path" */).then(c => wrapFunctional(c.default || c)),
  EndkMenu: () => import('../../components/endkMenu.vue' /* webpackChunkName: "components/endk-menu" */).then(c => wrapFunctional(c.default || c)),
  EndkSimpleCar: () => import('../../components/endkSimpleCar.vue' /* webpackChunkName: "components/endk-simple-car" */).then(c => wrapFunctional(c.default || c)),
  EndkSmallTexture: () => import('../../components/endkSmallTexture.vue' /* webpackChunkName: "components/endk-small-texture" */).then(c => wrapFunctional(c.default || c)),
  SectionsEndkSection3: () => import('../../components/sections/endk-section-3.vue' /* webpackChunkName: "components/sections-endk-section3" */).then(c => wrapFunctional(c.default || c)),
  SectionsEndkSectionColeccion: () => import('../../components/sections/endk-section-coleccion.vue' /* webpackChunkName: "components/sections-endk-section-coleccion" */).then(c => wrapFunctional(c.default || c)),
  SectionsEndkSectionContacto: () => import('../../components/sections/endk-section-contacto.vue' /* webpackChunkName: "components/sections-endk-section-contacto" */).then(c => wrapFunctional(c.default || c)),
  SectionsEndkSectionInicio: () => import('../../components/sections/endk-section-inicio.vue' /* webpackChunkName: "components/sections-endk-section-inicio" */).then(c => wrapFunctional(c.default || c)),
  SectionsEndkSectionMitigacion: () => import('../../components/sections/endk-section-mitigacion.vue' /* webpackChunkName: "components/sections-endk-section-mitigacion" */).then(c => wrapFunctional(c.default || c)),
  SectionsEndkSectionNosotros: () => import('../../components/sections/endk-section-nosotros.vue' /* webpackChunkName: "components/sections-endk-section-nosotros" */).then(c => wrapFunctional(c.default || c)),
  SectionsEndkSectionProyectos: () => import('../../components/sections/endk-section-proyectos.vue' /* webpackChunkName: "components/sections-endk-section-proyectos" */).then(c => wrapFunctional(c.default || c))
}

for (const name in components) {
  Vue.component(name, components[name])
  Vue.component('Lazy' + name, components[name])
}
