# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<EndkCursor>` | `<endk-cursor>` (components/endkCursor.vue)
- `<EndkCurveText>` | `<endk-curve-text>` (components/endkCurveText.vue)
- `<EndkDataScroll>` | `<endk-data-scroll>` (components/endkDataScroll.vue)
- `<EndkLargePath>` | `<endk-large-path>` (components/endkLargePath.vue)
- `<EndkMenu>` | `<endk-menu>` (components/endkMenu.vue)
- `<EndkSimpleCar>` | `<endk-simple-car>` (components/endkSimpleCar.vue)
- `<EndkSmallTexture>` | `<endk-small-texture>` (components/endkSmallTexture.vue)
- `<SectionsEndkSection3>` | `<sections-endk-section3>` (components/sections/endk-section-3.vue)
- `<SectionsEndkSectionColeccion>` | `<sections-endk-section-coleccion>` (components/sections/endk-section-coleccion.vue)
- `<SectionsEndkSectionContacto>` | `<sections-endk-section-contacto>` (components/sections/endk-section-contacto.vue)
- `<SectionsEndkSectionInicio>` | `<sections-endk-section-inicio>` (components/sections/endk-section-inicio.vue)
- `<SectionsEndkSectionMitigacion>` | `<sections-endk-section-mitigacion>` (components/sections/endk-section-mitigacion.vue)
- `<SectionsEndkSectionNosotros>` | `<sections-endk-section-nosotros>` (components/sections/endk-section-nosotros.vue)
- `<SectionsEndkSectionProyectos>` | `<sections-endk-section-proyectos>` (components/sections/endk-section-proyectos.vue)
